#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    double getScore();
    double getScore(bool override);
    Side getPlayer();
   	Side getOpp();

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    Side player_side;
    Board *board;
};

#endif
