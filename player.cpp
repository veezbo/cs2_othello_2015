#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    player_side = side;
    srand(time(NULL));

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

     board = new Board();

     //test change
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

double Player::getScore()
{
    return getScore(false);
}

double Player::getScore(bool override)
{
    if (override || testingMinimax)
    {
        return board->count(getPlayer()) - board->count(getOpp());
    }
    else
    {
        return board->count(getPlayer()) - board->count(getOpp())
        + (3. * board->countCorners(getPlayer()) - 1. * board->countCorners(getOpp()))
        - (1. * board->countAdjacentCorner(getPlayer()) - 3.* board->countAdjacentCorner(getOpp()));
    }
    
}

Side Player::getOpp()
{
    if (player_side == WHITE)
    {
        return BLACK;
    }
    else
    {
        return WHITE;
    }
}

Side Player::getPlayer()
{
    return player_side;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 

    if (player_side == WHITE)
    {
        board->doMove(opponentsMove, BLACK);
    }
    else
    {
        board->doMove(opponentsMove, WHITE);
    }

    if (!board->hasMoves(player_side))
    {
        // std::cerr << "No moves left" << endl;
        return NULL;
    }

    //Random Selection of Move
    //--------------------------------------------
    // int x, y;
    // Move *m = new Move(-1, -1);
    // do
    // {
    //     x = rand() % 8;
    //     y = rand() % 8;
    //     m->setX(x);
    //     m->setY(y);
    // }
    // while (!board->checkMove(m, player_side));
    //--------------------------------------------


    //First-level deep selection of move (consistently beats SimplePlayer)
    //--------------------------------------------------------------------
    // Move *m = new Move(-1, -1);
    // int maxScore = -1000;
    // Board *prev = board->copy();
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         Move *test = new Move(i, j);
    //         if (!board->checkMove(test, getPlayer()))
    //         {
    //             delete test;
    //             continue;
    //         }
    //         board->doMove(test, player_side);
    //         int score = getScore();
    //         if (score > maxScore)
    //         {
    //             maxScore = score;
    //             // std::cerr << "Score: " << score << std::endl;
    //             delete m;
    //             m = test;
    //         }
    //         else
    //         {
    //             delete test;
    //         }
    //         delete board;
    //         board = prev->copy();
    //     }
    // }
    // delete prev;
    //--------------------------------------------------------------------
    

    //Second-level deep selection of move
    //--------------------------------------------------------------------
    // Move *m = new Move(-1, -1);
    // double maxScore = -1000.;
    // Board *prev = board->copy();
    // for (int i = 0; i < 8; i++) for (int j = 0; j < 8; j++)
    // {
    //     Move *test = new Move(i, j);
    //     if (!board->checkMove(test, getPlayer()))
    //     {
    //         delete test;
    //         continue;
    //     }
    //     board->doMove(test, getPlayer());
    //     double minScore = 1000;
    //     Board *prev_1 = board->copy();
    //     for (int k = 0; k < 8; k++) for (int m = 0; m < 8; m++)
    //     {
    //         Move *temp_opp_move = new Move(k, m);
    //         if (!board->checkMove(temp_opp_move, getOpp()))
    //         {
    //             delete temp_opp_move;
    //             continue;
    //         }
    //         board->doMove(temp_opp_move, getOpp());
    //         delete temp_opp_move;
    //         double score = getScore();
    //         if (score < minScore)
    //         {
    //             minScore = score;
    //         }
    //         delete board;
    //         board = prev_1->copy();
    //     }
    //     delete prev_1;

    //     if (minScore > maxScore)
    //     {
    //         maxScore = minScore;
    //         delete m;
    //         m = test;
    //     }
    //     else
    //     {
    //         delete test;
    //     }
    //     delete board;
    //     board = prev->copy();
    // }
    // delete prev;
    //--------------------------------------------------------------------

    
    //Third-level deep selection of move, combined with a modified scoring function
    //Used for the AI, and supports the testing as well (short-circuits to two-layer in this case)
    //--------------------------------------------------------------------
    Move *move = new Move(-1, -1);
    double maxScore_1 = -10000.;
    Board *prev = board->copy();
    for (int i = 0; i < 8; i++) for (int j = 0; j < 8; j++)
    {
        Move *test = new Move(i, j);
        if (!board->checkMove(test, getPlayer()))
        {
            delete test;
            continue;
        }
        board->doMove(test, getPlayer());
        double minScore_2 = 10000.;
        Board *prev_1 = board->copy();
        for (int k = 0; k < 8; k++) for (int m = 0; m < 8; m++)
        {
            Move *temp_opp_move = new Move(k, m);
            if (!board->checkMove(temp_opp_move, getOpp()))
            {
                delete temp_opp_move;
                continue;
            }
            board->doMove(temp_opp_move, getOpp());
            delete temp_opp_move;
            // If no player move after opponent move, default to two-layer depth (if necessary,
            // hence the subtraction by 100, indicating we prefer three-layer selection).
            // Also, use two-layer depth if testingMinimax is specified (in this case, the 
            // subtraction by 100 is universal, and will not affect move selection)
            if (testingMinimax || !board->hasMoves(getPlayer()))
            {
                double score = getScore() - 100.;
                if (score < minScore_2)
                {
                    minScore_2 = score;
                }
                delete board;
                board = prev_1->copy();
                continue;
            }
            double maxScore_3 = -10000.;
            Board *prev_2 = board->copy();
            for (int n = 0; n < 8; n++) for (int p = 0; p < 8; p++)
            {
                Move *temp_opp_move_1 = new Move(n, p);
                if (!board->checkMove(temp_opp_move_1, getPlayer()))
                {
                    delete temp_opp_move_1;
                    continue;
                }
                board->doMove(temp_opp_move_1, getPlayer());
                delete temp_opp_move_1;
                double score = getScore();
                if (score < -10000.) fprintf(stderr, "Score less than very small\n");
                if (score > maxScore_3)
                {
                    maxScore_3 = score;
                }
                delete board;
                board = prev_2->copy();
            }
            if (maxScore_3 < minScore_2)
            {
                minScore_2 = maxScore_3;
            }
            delete board;
            board = prev_1->copy();
        }
        delete prev_1;

        if (minScore_2 > maxScore_1)
        {
            maxScore_1 = minScore_2;
            delete move;
            move = test;
        }
        else
        {
            delete test;
        }
        delete board;
        board = prev->copy();
    }
    delete prev;
    //--------------------------------------------------------------------


    //After getting the move from one of the above methods, we simply return
    //  after updating our own inner board
    board->doMove(move, player_side);
    return move;
}
